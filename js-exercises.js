/* Write a JavaScript program to converts a specified number to an array of digits. */

function numberToDigitArray(num) {
  const numbers = [];

  do {
    numbers.unshift(num % 10);
    num = Math.floor(num / 10);
  } while (num > 0);

  return numbers;
}

/* ******************************** */
/* Write a function to replace the values of multiple object key with the values provided. */

function replaceObjectKeyValues(original, replacementKeyValues) {
  for (const key in replacementKeyValues) {
    original[key] = replacementKeyValues[key];
  }

  /* 
  Alternative approach would be:
  original = { ...original, ...replacementKeyValues };
  but keep in mind that it creates a new object and re-assigns to original 
  */
}

replaceObjectKeyValues(
  { Name: 'Denis', Country: 'Israel' },
  { Name: 'Nir', City: 'Tel-Aviv' }
);

/* ******************************** */
/* Write a function to replace the names of multiple object keys with the values provided. */

function replaceObjectKeyNames(target, newKeyNames) {
  for (const oldKey in newKeyNames) {
    if (target.hasOwnProperty(oldKey)) {
      const newName = newKeyNames[oldKey];

      // To make replaced key value identical to the old one
      const oldPropDescriptor = Object.getOwnPropertyDescriptor(target, oldKey);

      Object.defineProperty(target, newName, oldPropDescriptor);

      delete target[oldKey];
    } else
      throw new Error(
        'Object does not have a given key name, thus it can not be replaced!'
      );
  }

  return target;
}

replaceObjectKeyNames({ Name: 'Denis', Country: 'Israel' }, { Name: 'Nir' });

/* ******************************** */
/* Write a function that merges 2 objects into a new object */

const mergeObjects = (obj1, obj2) => ({ ...obj1, ...obj2 });

mergeObjects({ a: 1, b: 2 }, { b: 3, c: 4 });

/* ******************************** */
/* Write a function that merges an array of objects into a new object */

function mergeObjectsFromArray(objects) {
  let merged = {};

  for (const target of objects) {
    merged = { ...merged, ...target };

    // It is elegant solution, but need to remember that it creates a new object instance every time
    // Maybe slightly faster but older approach would be: Object.assign(merged, target);
  }

  return merged;
}

mergeObjectsFromArray([{ a: 1, b: 2 }, { b: 3, c: 4 }, { c: 5, d: 6 }]);

/* ******************************** */
/* Write a function that filters an array of objects by the value of the key ‘name’ */

function filterArrayBy(objArr, filter) {
  return objArr.filter(o => o.hasOwnProperty('name') && o.name.includes(filter));
}

filterArrayBy(
  [{ name: 'Denis', age: 27 }, { name: 'Nir', country: 'Israel' }, { age: 27 }],
  'N'
);

/* ******************************** */
/* Write a function that takes a number as an argument and returns a Promise
that tests if the value is less than or greater than the value 10.
If the value is greater than 10, the promise is resolved, otherwise its rejected. */

function isGreaterThanTen(num) {
  return new Promise((res, rej) => {
    if (num > 10) res(num);
    rej(num);
  });
}

isGreaterThanTen(15)
  .then(num => console.log('Resolved', num))
  .catch(num => console.log('Rejected', num));


/* ******************************** */
  /* Given this array [1,2,3], create 3 new variables with the names
one, two, three that will contain the values accordingly. */

const threeNumbers = [1, 2, 3];

const [one, two, three] = threeNumbers;

/* Declare 3 new variables from the given array [1,2,3,5,6].
The first variable should contain the first value,
the second should contain the second,
and the last should contain the rest of the values as an array. */

const fiveNumbers = [1, 2, 3, 4, 5];

const [first, second, ...rest] = fiveNumbers;

/* Write a function that takes an object and a parameter called newrow,
and adds the newrow parameter to a key called payload. */

function addToPayload(object, newrow) {
  const { payload } = object;

  object.payload = { ...payload, newrow };

  return object;
}

addToPayload(
  { name: 'Denis', country: 'Israel', payload: { data: 'from_newrow' } },
  '_newrow'
);
